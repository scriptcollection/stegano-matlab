clear all;  % Clear the existing workspace 

clc;  %Clear the command window 

file_img1 = 'img1.tiff'; %setting file image 1
file_img2 = 'img2.tiff'; %setting file image 2

img1 = imread(file_img1); %membaca file image1
img2 = imread(file_img2); %membaca file image2

bitxor_img = bitxor(img1,img2); %membandingkat bit image 1 dan 2

newbitor = bitxor_img(1:9,1:512); %mengambil baris data yang memiliki nilai 1

bindata = mat2str(newbitor); %mengubah data matrix menjadi string

%menghapus karakter spasi, koma, dan titik koma 
%(katakter yang terbawa dari metrix)
bindata = strrep(bindata(2:9216),' ','');
bindata = strrep(bindata,',','');
bindata = strrep(bindata,';','');

n = 8; %jumlah bit dalam 1 karakter
A = cellstr(reshape(bindata,n,[])') %membuat array dari binary data dengan membagi menjadi 8 bit setiap bagiannya
ns = numel(A); %mengambil panjang array karakter

%menggunakan looping untuk mengubah setiap binary ke decimal pada variable A, 
%dan di assign ke variable word
for k=1:ns
  word{k} = char(bin2dec(A{k}));
end

clc;  %Clear the command window 

%menampilkan seluruh data variable word dengan metode join
%karena bentuk variable word adalah array
disp(join(word,'')); 
