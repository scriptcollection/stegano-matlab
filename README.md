# Stegano-Matlab


*  Baris 1 & 2 untuk membersihkan layer dah variable dari memori.
*  Selanjutnya membuka file img1.tiff dan img2.tiff sebagai input image
*  Lalu melakukan XOR pada kedua input image tersebut.
*  Seletalh memperhatikan hasil XOR, didapati baris yang memiliki nilai 1 adalah baris 1 – 9.
*  Selanjutnya mengambil data pada baris 1 s.d. 9, kemudian mengubah data dalam bentuk metrix ke dalam string.
*  Mereplace seluruh karakter yang terbawa saat mengubah bentuk data metrix menjadi string ( spasi, koma, dan titik koma)
*  Sehingga didapatlan text sebagai berikut :

“In statistics, the mean squared error (MSE) or mean squared deviation (MSD) of an estimator (of a procedure for estimating an unobserved quantity) measures the average of the squares of the errorsthat is, the average squared difference between the estimated values and the actual value. MSE is a risk function, corresponding to the expected value of the squared error loss. The fact that MSE is almost always strictly positive (and not zero) is because of randomness or because the estimator does not account for information that could produce a more accurate estimate.”
